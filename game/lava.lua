function initLava()
	lava = {}
	registerList(agent_lava, lava)

	local lavacolor = {128,0,0}
	for i = 1,24 do
		newAgent(agent_lava, lavacolor)
	end
end

function drawLava()
	drawAgentList(lava)
end

function isLava(x,y)
	if lava then
		return isAgent(lava,x,y)
	end
	return false
end