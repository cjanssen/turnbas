function moveAgent(agent, order)
	local valid = false
	-- if order == order_none then
	-- 	return false
	-- end
	if order == order_left and agent.x > 0 then
		agent.dx = agent.x - 1
		valid = true
	end
	if order == order_right and agent.x < gridW-1 then
		agent.dx = agent.x + 1
		valid = true
	end
	if order == order_up and agent.y > 0 then
		agent.dy = agent.y - 1
		valid = true
	end
	if order == order_down and agent.y < gridH-1 then
		agent.dy = agent.y + 1
		valid = true
	end

	newAnim(agent)

	return valid
end

function evalDir(x, y, order)
	local nx,ny = x,y
	if order == order_left then
		nx = nx - 1
	end
	if order == order_right then
		nx = nx + 1
	end
	if order == order_up then
		ny = ny - 1
	end
	if order == order_down then
		ny = ny + 1
	end

	return nx,ny
end

function newAgentIn(at, _x, _y, c)
	local na = { agentType = at, x=_x, y=_y, color = c, active = true, dying = false }
	na.dx = _x
	na.dy = _y
	na.cx = _x
	na.cy = _y
	return na
end

function newAgent(at, c)
	local na = { agentType = at, color = c, active = true, dying = false }
	repeat
		na.x,na.y = math.random(gridW)-1, math.random(gridH)-1
	until not occupied(na.x, na.y) 

	na.dx = na.x
	na.dy = na.y
	na.cx = na.x
	na.cy = na.y

	table.insert(listForType(at), na)
	return na
end

function listForType(at)
	return AllLists[at]
end

function isAgent(list,x,y)
	for i,v in ipairs(list) do
		if v.active and v.x==x and v.y==y then return v end 
	end	
	return nil
end

function drawRect(x,y)
	love.graphics.rectangle("fill",x*cellSize,y*cellSize,cellSize,cellSize)
end

function drawAgent(agent)
	love.graphics.setColor(agent.color[1],agent.color[2],agent.color[3])
	drawRect(agent.cx, agent.cy)
end

function drawAgentList(list)
	for i,v in ipairs(list) do
		if v.active then
			drawAgent(v)
		end
	end
end

function initLists()
	AllLists = {}
end

function registerList(agent_type, list)
	table.insert(AllLists, list)
	AllLists[agent_type] = list
end

function occupied(x,y)
	-- if player.x == x and player.y == y then return true end
	for i,v in ipairs(AllLists) do
		local ag = isAgent(v, x, y)
		if ag then return ag end
	end
	return false
end

function nextToAnything(agent, dir)
	for i,v in ipairs(AllLists) do
		local ag = nextToAgent(v, agent, dir)
		if ag then return ag end
	end
	return nil
end

function willBeOccupied(x,y)
	for i,v in ipairs(AllLists) do
		for j,w in ipairs(v) do
			if w ~= player and w.active and w.dx == x and w.dy == y then return true end
		end
	end
	return false
end

function removeFromList(list, what)
	for i,v in ipairs(list) do
		if v == what then
			table.remove(list, i)
		end
	end
end

function substractList(listFrom, listWhat)
	for i,v in ipairs(listWhat) do
		removeFromList(listFrom, v)
	end
end

function isOffLimits(x,y)
	if x<0 then return true end
	if x>=gridW then return true end
	if y<0 then return true end
	if y>=gridH then return true end
	return false
end

function nextToAgent(list, agent, dir)
	for i,v in ipairs(list) do
		if v.active then
			if dir == order_right and agent.x == v.x - 1 and agent.y == v.y then
				return v
			end
			if dir == order_left and agent.x == v.x + 1 and agent.y == v.y then
				return v
			end
			if dir == order_up and agent.x == v.x and agent.y == v.y +1 then
				return v
			end
			if dir == order_down and agent.x == v.x and agent.y == v.y - 1 then
				return v
			end
		end
	end
	return nil
end

function sortListBy(list, field)
	function compare(a,b)
		return a[field] < b[field]
	end
	table.sort(list, compare)
end

function harmattan(a, b)
	return math.abs(a.x-b.x)+math.abs(a.y-b.y)
end

function randomizeList(list)
	local l = {}
	repeat
		table.insert(l,table.remove(list,math.random(table.getn(list))));
	until table.getn(list) == 0
	return l
end

function reverseOrder(order)
	if order == order_up then
		return order_down
	end

	if order == order_down then
		return order_up
	end

	if order == order_left then
		return order_right
	end

	if order == order_right then
		return order_left
	end
end

function killAgent(agent)
	local canDie = agent.active
	if agent.active then
		if agent.agentType == agent_lava then
			canDie = false
			-- lava can't die
		end
		if agent.agentType == agent_player then
			die()
		end
		if agent.agentType == agent_bomb then
			explodeBomb(agent)
		end
		if agent.agentType == agent_box then
			destroyBox(agent)
		end
		if agent.agentType == agent_enemy then
			killEnemy(agent)
		end
		if canDie then
			agent.active = false
		end
	end
end