
function initPlayer()
	local playerAsList = {}
	registerList(agent_player, playerAsList)
	player = newAgent (agent_player, {0,0,128} )
	player.pulling = false
	player.throwing = false
	player.putbomb = false
end

function isPlayer(x,y)
	if player.x == x and player.y == y then
		return true
	end
	return false
end

function drawPlayer()
	drawAgent(player)
end

function movePlayer(order)
	local valid = moveAgent(player, order)

	if valid and isLava(player.x, player.y) then
		die()
		valid = false
	end

	return valid
end

function putBomb(order)
	local ok = newBomb(player.x, player.y, order)
	player.putbomb = false
	return ok
end