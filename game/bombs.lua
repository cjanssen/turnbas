function initBombs()
	bombs = {}
	explo = {}
	registerList(agent_bomb, bombs)
	registerList(agent_explosion, explo)
end

function newBomb(x, y, dir)
	local newBomb = newAgentIn(agent_bomb, x,y, {0,32,64})
	newBomb.canCrush = false
	newBomb.range = 2
	newBomb.timer = newBomb.range * 2 + 1

	-- try to place the bomb in location
	local ok = not nextToAnything(newBomb, dir) and moveAgent(newBomb, dir)
	-- return true if successful
	if ok then
		table.insert(bombs, newBomb)
		table.insert(boxes, newBomb)
	else
		newBomb.active = false
	end

	return ok
end

function execBombs()
	for i,v in ipairs(bombs) do
		if v.active then
			advanceBomb(v)
		end
	end
end

function advanceBomb(bomb)
	bomb.timer = bomb.timer - 1
	if bomb.timer == 1 then
		bomb.color = {0,64,128}
	end
	if bomb.timer == 0 then
		explodeBomb(bomb)
	end
end

function explodeBomb(bomb)
	bomb.active = false

	for xx = bomb.x-bomb.range,bomb.x+bomb.range do
		for yy = bomb.y-bomb.range,bomb.y+bomb.range do
			if harmattan(bomb,{x = xx,y = yy}) <= bomb.range and not isOffLimits(xx,yy) then
				local victim = occupied(xx,yy)
				if victim then
					-- if not isLava(xx,yy) then
					-- 	if victim == player then
					-- 		die()
					-- 	end
					-- 	if victim.active and victim.itsABomb then
					-- 		explodeBomb(victim)
					-- 	end
						killAgent(victim)
						-- victim.active = false
					-- end
				end
				local explCell = newAgentIn(agent_explosion, xx,yy, {192,192,192})
				table.insert(explo,explCell)
				
			end
		end
	end
end

function clearExplosions()
	local n = table.getn(explo)
	for i=1,n do
		table.remove(explo,1)
	end
end

function drawBombs()
	-- draw bombs -- counts as box
	-- drawAgentList(bombs)
	-- draw explosions
	drawAgentList(explo)
end

function isBomb(x,y)
	return isAgent(bombs,x,y)
end
