-- todo:
-- preview where an enemy is going to move
-- explosion: wait a turn until killing player (so that he han see it)
-- types of enemies: soft, hard, hardest (crush against different surfaces)
-- more ai?
-- pg level?
-- periodic rain of blocks
-- level zooms out when cleared, shows more stuff in the surroundings
-- powerups behind boxes?
-- types of bombs?
-- somehow go in the turing machine direction??
-- push rows of blocks?
-- extra turn (fast move)
-- fire (burns series of blocks)
-- water (barricades enemies but you can walk through)
-- ice (slippery)
-- dig (opens lava in the floor)
-- options screen (you enable/disable features, for testing)
-- enemies that jump/teleport
-- enemies that can kill from a neighbour cell (hit you from your side)
-- enemies that walk straight and then turn
-- enemies that spit new blocks
-- enemies that push blocks
-- chain reactions of pushes? (block to block)
-- nests (spawn more enemies)



function initEnemies()
	enemies = {}
	registerList(agent_enemy, enemies)

	for i=1,40 do
		local enemy = newAgent(agent_enemy, {192,0,128})
		enemy.type = 1
		enemy.turnMax = getRandomSpeedForEnemy()
		enemy.turn = math.random(enemy.turnMax)
		recolorEnemy(enemy)
	end

	live_enemies = table.getn(enemies)
end

function getRandomSpeedForEnemy()
	local i = math.random(3)
	if math.random(2)>1 then 
		return enemyspeed_normal
	elseif math.random(2)>1 then
		return enemyspeed_slow
	end
	return enemyspeed_fast
end

function drawEnemies()
	drawAgentList(enemies)
end

function execEnemies()
	local someoneMoved = false
	for i,v in ipairs(enemies) do
		if v.active then
			someoneMoved = evalEnemy(v) or someoneMoved
		end
	end
	return someoneMoved
end

function evalEnemy(e)
	-- move
	local thisMoved
	e.turn = e.turn - 1
	if e.turn <= 0 then
		thisMoved = true
		moveEnemy(e)
		e.turn = e.turnMax
	end

	recolorEnemy(e)
	return thisMoved
end

function recolorEnemy(e)
	-- recolor
	if e.turn == 1 then 
		e.color = {192,0,128}
	else
		if e.turnMax == enemyspeed_slow then
			e.color = {76,0,51}
		elseif e.turnMax == enemyspeed_normal then
			e.color = {117,4,80}
		elseif e.turnMax == enemyspeed_fast then
			e.color = {155,13,108}
		end
	end
end

function moveEnemy(e)
	local candidates = { 
		{ x = e.x - 1, y = e.y, dir = order_left },
		{ x = e.x + 1, y = e.y, dir = order_right },
		{ x = e.x, y = e.y - 1, dir = order_up },
		{ x = e.x , y = e.y + 1, dir = order_down }
	}

	-- evaluate possibilities
	local purge = {}
	local decision = false
	for i,v in ipairs(candidates) do
		v.dist = harmattan(player, v)
		if isPlayer(v.x, v.y) then decision = v else
			if occupied(v.x, v.y) then table.insert(purge,v) end
			if willBeOccupied(v.x, v.y) then table.insert(purge,v) end
		end
		if isOffLimits(v.x, v.y) then table.insert(purge,v) end
	end

	-- decision
	substractList(candidates, purge)
	if table.getn(candidates) == 0 then return end
	if not decision then
		if harmattan(player, e) < 7 then
			candidates = randomizeList(candidates)
			sortListBy(candidates, "dist")
			decision = candidates[1]
		else
			local i = math.random(table.getn(candidates))
			decision = candidates[i]
		end
	end

	-- actual move
	moveAgent(e, decision.dir)
end

function checkPlayerKilled()
	for i,v in ipairs(enemies) do
		if v.active and isPlayer(v.x, v.y) then return true end
	end
end

function nextToEnemy(agent, dir)
	return nextToAgent(enemies, agent, dir)
end

function pushEnemy(enemy, dir)
	local x,y = evalDir(enemy.x, enemy.y, dir)
	local enemyDead = false
	if isBomb(x,y) then
		return false
	end

	if isOffLimits(x,y) or occupied(x,y) then
		killEnemy(enemy)
	end
	return moveAgent(enemy, dir)
end

function killEnemy(e)
	live_enemies = live_enemies - 1
	e.dying = true
end

