function initBoxes()
	boxes = {}
	registerList(agent_box, boxes)

	for i=1,240 do
		local fixed = false
		if math.random(4) == 1 then fixed = true end
		local cracked = false
		if not fixed and math.random(5) == 1 then cracked = true end
		local color = {128,96,0}
		if not fixed then color = {144, 72, 0} end
		if cracked then color = {96, 48, 0} end

		local newBox = newAgent(agent_box, color)
		newBox.fixed = fixed
		newBox.cracked = cracked
		newBox.canCrush = not fixed
	end


end

function isBox(x,y)
	if boxes then
		return isAgent(boxes,x,y)
	end
	return false
end

function drawBoxes()
	drawAgentList(boxes)
end

function nextToBox(agent, dir)
	return nextToAgent(boxes, agent, dir)
end

function moveBox(box, dir)
	if not box.active then return false end
	if box.fixed then return false end
	if box.cracked then 
		-- note: this assumes that "moveBox" will be called only when player steps on it
		destroyBox(box)
		dir = order_none
	end

	if nextToBox(box, dir) then
		return false
	end

	local valid = true
	local en = nextToEnemy(box, dir)
	if en then
		if box.thrown then
			-- stop here
			dir = order_none
		else
			if box.canCrush then
				valid = pushEnemy(en, dir)
			else
				dir = order_none
			end
		end
	end

	valid = valid and moveAgent(box, dir)
	if valid then
		if isLava(box.dx, box.dy) then
			if box.agentType == agent_bomb then
				-- todo: at the end of the animation!
				explodeBomb(box)
			else
				destroyBox(box)
			end	
		end
	end
	return valid
end

function destroyBox(box)
	-- box.active = false
	box.dying = true
end
