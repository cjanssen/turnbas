order_none = 0
order_left = 1
order_right = 2
order_up = 3
order_down = 4

game_normal = 0
game_lost = 1
game_won = 2


enemyspeed_normal = 3
enemyspeed_fast = 2
enemyspeed_slow = 4
enemystrength_weak = 1
enemystrength_hard = 2
enemystrength_hardest = 3

agent_player = 1
agent_box = 2
agent_bomb = 3
agent_explosion = 4
agent_lava = 5
agent_enemy = 6

cellSize = 25
gridW = 40
gridH = 28