function initAnims()
	anims = { }
	animCtrl = { running = false, frac = 0, delay = 0.1 }
end

function newAnim(agent)
	table.insert(anims, agent)
end

function updateAnims(dt)
	if table.getn(anims) > 0 and not animCtrl.running then
		animCtrl.running = true
		animCtrl.frac = 0
	end

	if animCtrl.running then
		if animCtrl.frac >= 1 then 
			local actors = anims
			initAnims()
			for i,a in ipairs(actors) do
				a.x,a.y = a.dx,a.dy
				a.cx,a.cy = a.x,a.y
				if a.postAnim then
					a.postAnim()
				end
				if a.dying then
					a.active = false
				end
			end
			if table.getn(anims) == 0 then
				animsDone()
			end
		else
			animCtrl.frac = animCtrl.frac + dt / animCtrl.delay
			for i,a in ipairs(anims) do
				a.cx = (a.dx - a.x) * animCtrl.frac + a.x
				a.cy = (a.dy - a.y) * animCtrl.frac + a.y
			end
		end
	end
end