
function include( filename )
	love.filesystem.load( filename )()
end

function love.load()
	include("constants.lua")
	include("agent.lua")
	include("player.lua")
	include("boxes.lua")
	include("lava.lua")
	include("enemies.lua")
	include("anims.lua")
	include("bombs.lua")

	love.window.setMode(1000,700)
	gameFont = love.graphics.newFont(48)
	normalFont = love.graphics.newFont(12)
	initGame()
end

function initGame()
	help_shown = false
	gameState = game_normal
	initLists()
	initPlayer()
	initBombs()
	initBoxes()
	initLava()
	initEnemies()
	initAnims()
	enemyTurn = false
	buffered_orders = {}
end

function love.draw()
	if gameState == game_normal then
		-- background
		love.graphics.setColor(16,16,16)
		love.graphics.rectangle("fill",0,0,cellSize*gridW, cellSize*gridH)

		drawLava()
		drawEnemies()
		drawPlayer()
		drawBoxes()
		drawBombs()
		drawHelp()
	elseif gameState == game_won then
		love.graphics.setColor(24,24,24)
		love.graphics.setFont(gameFont)
		love.graphics.print("YOU WIN", 200, 200)
		love.graphics.print("press return", 200, 248)
	elseif gameState == game_lost then
		love.graphics.setColor(24,24,24)
		love.graphics.setFont(gameFont)
		love.graphics.print("GAME OVER", 200, 200)
		love.graphics.print("press return", 200, 248)
	end

end

function drawHelp()
	love.graphics.setColor(255,255,255)
	love.graphics.setFont(normalFont)
	if not help_shown then
		love.graphics.print("F1:toggle help",2,2)
	else
		love.graphics.print("F1:toggle help - arrows:move - X+arrows:throw - C+arrows:pull - V:putbomb - R:restart - ESC:exit",2,2)
		love.graphics.print("red:lava - purple:monster - yellow:wall - orange:movable - brown:breakable - lightblue:bomb",2,18)
	end
end

function love.update(dt)
	updateAnims(dt)
end

function die()
	gameState = game_lost
end

function checkWin()
	if live_enemies == 0 then
		winGame()
	end
end

function winGame()
	gameState = game_won
end

function animsDone()
	if checkPlayerKilled() then
		die()
	end

	if gameState == game_normal then
		checkWin()
	end

	local execBuffered = false
	if not enemyTurn then
		execBombs()
		local someoneMoved = execEnemies()
		if not someoneMoved then
			execBuffered = true
		end
	else
		execBuffered = true
		clearExplosions()
	end

	if execBuffered and table.getn(buffered_orders)>0 then
		local newOrder = table.remove(buffered_orders, 1)
		execTurn(newOrder)
	end

	enemyTurn = not enemyTurn
end

function execTurn(order)
	local orderValid = false

	if player.putbomb then
		orderValid = putBomb(order)
	else
		local box = nextToBox(player, order)
		if box then
			orderValid = moveBox(box, order)
			if orderValid then
				if player.throwing then
					box.thrown = true
					box.postAnim = function()
						if not moveBox(box, order) then
							box.postAnim = nil
							box.thrown = false
						end 
					end
				end
				orderValid = movePlayer(order)
			end
		else
			orderValid = movePlayer(order)
		end

		if orderValid and player.pulling then
			local pulledBox = nextToBox(player,reverseOrder(order))
			if pulledBox then
				moveBox(pulledBox, order)
			end
		end
	end

	if not orderValid then
		return
	end

	-- execEnemies()
end

function love.keypressed(key)
	local order = order_none

	if gameState == game_normal then
		
		if key == "left" then
			order = order_left
		end

		if key == "right" then
			order = order_right
		end
		
		if key == "up" then
			order = order_up
		end
		
		if key == "down" then
			order = order_down
		end

		if key == "x" then
			player.throwing = true
		end

		if key == "c" then
			player.pulling = true
		end

		if key == "v" then
			player.putbomb = true
		end

		if order ~= order_none then
			if animCtrl.running then
				table.insert(buffered_orders, order)
			else
				execTurn(order)
			end
		end

		if key == "r" then
			initGame()
		end

		if key =="z" then
			initAnims()
		end

		if key == "f1" then
			help_shown = not help_shown
		end
	else
		if key == "return" then
			initGame()
		end
	end

	if key == "escape" then
		love.event.push("quit")
	end
end

function love.keyreleased(key)
	if key == "x" then
		player.throwing = false
	end

	if key == "c" then
		player.pulling = false
	end
end

